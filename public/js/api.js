// Initialize Cloud Functions through Firebase
$(document).ready(function () {

    $.fn.serializeObject = function() {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    let functions = firebase.functions();
    const signupCall = functions.httpsCallable("signup");


    if (window.location.href.indexOf("localhost") > -1)
    {
        functions.useFunctionsEmulator('http://localhost:5001');
        console.log("using local emulator");
    }

    let quickSignup = () => {
        let email = $('#inscreva-se .form-group input[name$="email"]').val();
        let name = $('#inscreva-se .form-group input[name$="name"]').val();
        let surname = $('#inscreva-se .form-group input[name$="surname"]').val();
        let checked = $('#inscreva-se .form-group input[name$="quick-agree"]').is(":checked");
        if(checked) {
            signupCall({email,name,surname}).then((result) => {
                $('#inscreva-se .form-group input[name$="email"]').val('');
                $('#inscreva-se .form-group input[name$="name"]').val('');
                $('#inscreva-se .form-group input[name$="surname"]').val('');
                alert("Suas informações foram enviados com sucesso!");
                
            });
        } else {
            alert("Para continuar você deve aceitar os Termos de Privacidade e Uso"); //TODO: mudar isso
        }
    }

    let webinarSubmit = (e) => {
        e.preventDefault();
        let form = $("#webinarForm").serializeObject();
        console.log(form);
        let checked = $("#terms-acc").is(":checked");
        if(checked);
        signupCall(form).then((result) => {
            var frm = document.getElementById("webinarForm");
            frm.reset();
            alert("Suas informações foram enviados com sucesso!");
        })
        console.log(checked);
    }

    $("#submit-it").on("click", quickSignup);
    $("#submit").on("click", webinarSubmit);



});
