const functions = require('firebase-functions');
const admin = require('firebase-admin');
const express = require('express');
const cors = require('cors');

const app = express();

// Automatically allow cross-origin requests
app.use(cors({ origin: true }));
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

var serviceAccount = require("./sak.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://nerdblog-15198.firebaseio.com",
    firebase: functions.config().firebase
});
const db = admin.firestore();

const signup = async (data, context) => {
    let email = data.email;
    console.log(email);
    try {
        await db.collection("users").doc(email).set(data);
        return "OK";
    } catch (e) {
        console.log(e.message);
        return "Falhou";
    }
};

const setPost = async (req, res) => {
    try {
        await db.collection("posts").doc(req.params.id).set(req.body);
        return res.status(200).send("OK");
    } catch (e) {
        console.log(e.message);
        return res.status(500).send("Nao adicionado");
    }
};

const getPost = async (data, context) => {
    try {
        let snapshot = await db.collection("posts").doc(data.id);
        let post = await snapshot.get();
        return post.data();
    } catch (e) {
        console.log(e.message);
        return "Falhou";
    }
}

const getPosts = async (data, context) => {
    try {
        let snapshot = await db.collection("posts").get();
        let posts = await snapshot.docs.map(x => x.data());
        return post;
    } catch (e) {
        console.log(e.message);
        return "Falhou";
    }
}

// app.post("/signup",signup);

app.post("/:id", setPost);

// app.get("/posts/:id", getPost);

// exports.signup = functions.https.onCall(signup);
exports.setPost = functions.https.onRequest(app);
exports.getPost = functions.https.onCall(getPost);
exports.getPosts = functions.https.onCall(getPosts);
exports.signup = functions.https.onCall(signup);